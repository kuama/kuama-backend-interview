# Kuama Backend Interview

N.B. You can write the project in the language and with the frameworks that you prefer.

Given the following json:

```json
{
  "details": [
    {
      "payment_type": "priority",
      "beneficiary_entity_type": "individual",
      "fields": {
        "beneficiary_last_name": "^.{1,255}",
        "beneficiary_address": "^.{1,255}",
        "iban": "([A-Z0-9]\\s*){15,34}",
        "beneficiary_city": "^.{1,255}",
        "beneficiary_first_name": "^.{1,255}",
        "bic_swift": "^[0-9A-Z]{8}$|^[0-9A-Z]{11}$"
      }
    },
    {
      "payment_type": "priority",
      "beneficiary_entity_type": "company",
      "fields": {
        "beneficiary_company_name": "^.{1,255}",
        "beneficiary_address": "^.{1,255}",
        "iban": "([A-Z0-9]\\s*){15,34}",
        "beneficiary_city": "^.{1,255}",
        "bic_swift": "^[0-9A-Z]{8}$|^[0-9A-Z]{11}$"
      }
    },
    {
      "payment_type": "regular",
      "beneficiary_entity_type": "individual",
      "fields": {
        "iban": "([A-Z0-9]\\s*){15,34}"
      }
    },
    {
      "payment_type": "regular",
      "beneficiary_entity_type": "company",
      "fields": {
        "iban": "([A-Z0-9]\\s*){15,34}"
      }
    }
  ]
}
```

Create a project that exposes and endpoint that will read the json and serve it on a GET `api/payment-details?by=type` in the following form:

```json
{
  "data": {
    "types": [
      {
        "type": "priority",
        "values": [
          {
            "beneficiary_entity_type": "individual",
            "fields": {
              "beneficiary_last_name": "^.{1,255}",
              "beneficiary_address": "^.{1,255}",
              "iban": "([A-Z0-9]\\s*){15,34}",
              "beneficiary_city": "^.{1,255}",
              "beneficiary_first_name": "^.{1,255}",
              "bic_swift": "^[0-9A-Z]{8}$|^[0-9A-Z]{11}$"
            }
          },
          {
            "beneficiary_entity_type": "company",
            "fields": {
              "beneficiary_company_name": "^.{1,255}",
              "beneficiary_address": "^.{1,255}",
              "iban": "([A-Z0-9]\\s*){15,34}",
              "beneficiary_city": "^.{1,255}",
              "bic_swift": "^[0-9A-Z]{8}$|^[0-9A-Z]{11}$"
            }
          }
        ]
      },
      {
        "type": "regular",
        "values": [
          {
            "beneficiary_entity_type": "individual",
            "fields": {
              "iban": "([A-Z0-9]\\s*){15,34}"
            }
          },
          {
            "beneficiary_entity_type": "company",
            "fields": {
              "iban": "([A-Z0-9]\\s*){15,34}"
            }
          }
        ]
      }
    ]
  }
}


```

And another endpoint that will read the json and serve it on a GET `api/payment-details?by=beneficiary` in the following form:

```json
{
  "data": {
    "beneficiary_entity_type": [
      {
        "type": "individual",
        "values": [
          {
            "payment_type": "priority",
            "fields": {
              "beneficiary_last_name": "^.{1,255}",
              "beneficiary_address": "^.{1,255}",
              "iban": "([A-Z0-9]\\s*){15,34}",
              "beneficiary_city": "^.{1,255}",
              "beneficiary_first_name": "^.{1,255}",
              "bic_swift": "^[0-9A-Z]{8}$|^[0-9A-Z]{11}$"
            }
          },
          {
            "payment_type": "regular",
            "fields": {
              "iban": "([A-Z0-9]\\s*){15,34}"
            }
          }
        ]
      },
      {
        "type": "company",
        "values": [
          {
            "payment_type": "priority",
            "fields": {
              "beneficiary_company_name": "^.{1,255}",
              "beneficiary_address": "^.{1,255}",
              "iban": "([A-Z0-9]\\s*){15,34}",
              "beneficiary_city": "^.{1,255}",
              "bic_swift": "^[0-9A-Z]{8}$|^[0-9A-Z]{11}$"
            }
          },
          {
            "payment_type": "regular",
            "fields": {
              "iban": "([A-Z0-9]\\s*){15,34}"
            }
          }
        ]
      }
    ]
  }
}

```

Finally, a last endpoint PATCH `api/validate`, that will take in the following payload:
```json
{
    "beneficiary_company_name": "a company name",
    "beneficiary_address": "an address",
    "iban": "an iban",
    "beneficiary_city": "a city",
    "bic_swift": "a bic swift"
}
```

And will answer in the most common http-status that represents an invalid request the list of fields that are not valid (if any), or 200 OK if all the fields are valid.


## Bonus points
Write some tests!

## How to deliver
If you know how: clone this repository, create a separate branch and make a PR

Otherwise: will read the code from your machine
